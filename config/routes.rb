Rails.application.routes.draw do

  # mount_devise_token_auth_for 'User', at: 'api/v1/users/auth/'

  namespace :api do
    namespace :v1 do
      mount_devise_token_auth_for 'User', at: 'users/auth/'
      get 'enterprises', controller: 'enterprise', action: 'index'
      get 'enterprises/:id', controller: 'enterprise', action: 'find'
    end
  end
end
