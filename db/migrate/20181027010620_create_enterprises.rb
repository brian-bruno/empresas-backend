class CreateEnterprises < ActiveRecord::Migration[5.1]
  def change
    create_table :enterprises do |t|
      t.text :email_enterprise
      t.text :facebook
      t.text :twitter
      t.text :linkedin
      t.text :phone
      t.boolean :own_enterprise
      t.text :enterprise_name
      t.text :photo
      t.text :description
      t.text :city
      t.text :country
      t.float :value
      t.float :share_price

      t.timestamps
    end
  end
end
