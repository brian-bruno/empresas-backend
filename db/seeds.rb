# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

EnterpriseType.create(name: "Agro")
EnterpriseType.create(name: "Aviation")
EnterpriseType.create(name: "Biotech")
EnterpriseType.create(name: "Eco")
EnterpriseType.create(name: "Ecommerce")
EnterpriseType.create(name: "Education")
EnterpriseType.create(name: "Fashion")
EnterpriseType.create(name: "Fintech")
EnterpriseType.create(name: "Food")
EnterpriseType.create(name: "Games")
EnterpriseType.create(name: "Health")
EnterpriseType.create(name: "IOT")
EnterpriseType.create(name: "Logistics")
EnterpriseType.create(name: "Media")
EnterpriseType.create(name: "Mining")
EnterpriseType.create(name: "Enterprises")
EnterpriseType.create(name: "Estate")
EnterpriseType.create(name: "Service")
EnterpriseType.create(name: "City")
EnterpriseType.create(name: "Social")
EnterpriseType.create(name: "Software")
EnterpriseType.create(name: "Technology")
EnterpriseType.create(name: "Tourism")
EnterpriseType.create(name: "Transport")

Enterprise.create(enterprise_name: 'Sistemas Digitais', description: 'Lorem ipsum diam donec mollis leo, eget blandit curabitur etiam ullamcorper, pretium hac torquent sodales.', enterprise_type_id: 21)
Enterprise.create(enterprise_name: 'Sistemas Integrados', description: 'Lorem ipsum diam donec mollis leo, eget blandit curabitur etiam ullamcorper, pretium hac torquent sodales.', enterprise_type_id: 14)
Enterprise.create(enterprise_name: 'Sistemas Virtuais', description: 'Lorem ipsum diam donec mollis leo, eget blandit curabitur etiam ullamcorper, pretium hac torquent sodales.', enterprise_type_id: 21)
Enterprise.create(enterprise_name: 'Sistemas Físicos', description: 'Lorem ipsum diam donec mollis leo, eget blandit curabitur etiam ullamcorper, pretium hac torquent sodales.', enterprise_type_id:22)
Enterprise.create(enterprise_name: 'Sistemas em Nuvem', description: 'Lorem ipsum diam donec mollis leo, eget blandit curabitur etiam ullamcorper, pretium hac torquent sodales.', enterprise_type_id: 17)
Enterprise.create(enterprise_name: 'Sistemas Laravel', description: 'Lorem ipsum diam donec mollis leo, eget blandit curabitur etiam ullamcorper, pretium hac torquent sodales.', enterprise_type_id: 12)
Enterprise.create(enterprise_name: 'Sistemas Rails', description: 'Lorem ipsum diam donec mollis leo, eget blandit curabitur etiam ullamcorper, pretium hac torquent sodales.', enterprise_type_id: 10)
Enterprise.create(enterprise_name: 'Sistemas Brasileiros', description: 'Lorem ipsum diam donec mollis leo, eget blandit curabitur etiam ullamcorper, pretium hac torquent sodales.', enterprise_type_id: 24)
Enterprise.create(enterprise_name: 'Sistemas Globais', description: 'Lorem ipsum diam donec mollis leo, eget blandit curabitur etiam ullamcorper, pretium hac torquent sodales.', enterprise_type_id: 20)
Enterprise.create(enterprise_name: 'Sistemas Eletrônicos', description: 'Lorem ipsum diam donec mollis leo, eget blandit curabitur etiam ullamcorper, pretium hac torquent sodales.', enterprise_type_id: 21)
Enterprise.create(enterprise_name: 'Sistemas de Computadores', description: 'Lorem ipsum diam donec mollis leo, eget blandit curabitur etiam ullamcorper, pretium hac torquent sodales.', enterprise_type_id: 21)
Enterprise.create(enterprise_name: 'Sistemas Internacionais', description: 'Lorem ipsum diam donec mollis leo, eget blandit curabitur etiam ullamcorper, pretium hac torquent sodales.', enterprise_type_id: 21)
Enterprise.create(enterprise_name: 'Google', description: 'Lorem ipsum diam donec mollis leo, eget blandit curabitur etiam ullamcorper, pretium hac torquent sodales.', enterprise_type_id: 21)

User.create(email: 'testeapple@ioasys.com.br', uid: 'testeapple@ioasys.com.br', encrypted_password: '$2a$11$cXrJEqcTSwz6JOz1dcUs4eTBBh5J0VB0KfgKZqwF1RPphRh4lkyg.', provider: 'email', allow_password_change: 0, sign_in_count: 0, tokens: '{"IFZMv2A20VX_kV-YYDXgUw":{"token":"$2a$10$QrSsOXaof3IpWXwzbvSX7.k82moBYBrpHii4ui/wNIu7bev6kid5m","expiry":1541944110,"last_token":"$2a$10$.uWlCQSL6uNVR5Nx5xXKeujyPRFUxKROg9wsIP3D9otYAkwWuQDZG","updated_at":"2018-10-28T13:48:30.343Z"}}')