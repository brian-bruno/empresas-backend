class Api::V1::EnterpriseController < ApplicationController

  def index

    @enterprises = if params[:name].nil? || params[:enterprise_types].nil?
                     Enterprise.all.includes(:enterprise_type)
                   else
                     Enterprise.includes(:enterprise_type).where('enterprise_name like ? and enterprise_type_id = ?',
                                      '%' + params[:name] + '%', params[:enterprise_types])
                   end

    ActiveRecord::Base.include_root_in_json = false
    render json: { :enterprises => @enterprises }.to_json(:include => :enterprise_type), status: 200
    # render json: @enterprises.to_json(:include => [:enterprise_type]), status: 200
  end

  def find

    result = true
    begin
      @enterprise = Enterprise.find(params[:id])
    rescue => ex
      result = false
      @enterprise = nil
    end

    render json: {
      :enterprise => @enterprise,
      :success => result
    }.to_json(:include => :enterprise_type), status: 200
  end
end
