class CreateEnterpriseType < ActiveRecord::Migration[5.1]
  def change
    create_table :enterprise_types do |t|
      t.text :name
    end
  end
end
