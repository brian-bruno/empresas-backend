class AddEnterpriseTypeToEnterprises < ActiveRecord::Migration[5.1]
  def change
    add_column :enterprises, :enterprise_type_id, :integer, after: :share_price
    # add_foreign_key :enterprises, :enterprise_types
    # add_foreign_key :enterprises, :enterprise_types, column: :enterprise_type, primary_key: :id
  end
end
